package main

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"runtime/debug"
	"time"
)

func (app *application) serverError(w http.ResponseWriter, err error) {
	trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	app.errorLog.Output(2, trace)

	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

}

func (app *application) clientError(w http.ResponseWriter, status int) {
	// http.Error(w, http.StatusText(status), status)
	w.WriteHeader(status)
	if status == http.StatusNotFound {

		files := []string{
			"./ui/html/404.page.tmpl",
			"./ui/html/base.layout.tmpl",
			"./ui/html/leftsidebar.layout.tmpl",
			"./ui/html/rightsidebar.layout.tmpl",
			"./ui/html/slider.partial.tmpl",
			"./ui/html/topbar.partial.tmpl",
			"./ui/html/footer.partial.tmpl",
		}

		ts, _ := template.ParseFiles(files...)

		ts.Execute(w, nil)

	}
}

func (app *application) notFound(w http.ResponseWriter) {
	app.clientError(w, http.StatusNotFound)
}

func (app *application) addDefaultData(td *templateData, r *http.Request) *templateData {
	if td == nil {
		td = &templateData{}
	}
	td.CurrentYear = time.Now().Year()
	return td
}

func (app *application) render(w http.ResponseWriter, r *http.Request, name string, td *templateData) {
	ts, ok := app.templateCache[name]
	if !ok {
		app.serverError(w, fmt.Errorf("The template %s does not exist", name))
		return
	}

	buf := new(bytes.Buffer)

	err := ts.Execute(buf, app.addDefaultData(td, r))

	if err != nil {
		app.serverError(w, err)
	}

	buf.WriteTo(w)
}
